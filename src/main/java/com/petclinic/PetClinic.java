package com.petclinic;

import com.petclinic.service.InputOutputService;
import com.petclinic.service.OwnerService;
import com.petclinic.service.PetService;
import com.petclinic.service.VetService;

public class PetClinic {
    private InputOutputService inputOutputService;
    private VetService vetService;
    private OwnerService ownerService;
    private PetService petService;

    public PetClinic() {
        this.inputOutputService = new InputOutputService();
        this.vetService = new VetService(inputOutputService);
        this.ownerService = new OwnerService(inputOutputService);
        this.petService = new PetService(inputOutputService);
    }

    public void start() { //aici scriem logica de baza a aplicatiei
        while (true) {
            //pasul 1: afisam meniul
            inputOutputService.displayMainMenu();
            //pasul 2: luam input de la utilizator
            String userInput = inputOutputService.getUserInput();
            //pasul 3: procesam input-ul de la pasul 2
            process(userInput);
        }
    }

    private void process(String userInput) {
        switch (userInput) {
            case "1": {
                vetService.addVet();
                break;
            }
            case "2": {
                vetService.displayVets();
                break;
            }
            case "3": {
                vetService.modifyVet();
                break;
            }
            case "4": {
                vetService.removeVet();
                break;
            }
            case "5": {
                vetService.addHoliday();
                break;
            }
            case "6": {
                ownerService.addOwner();
                break;
            }
            case "7": {
                ownerService.displayOwners();
                break;
            }
            case "8": {
                ownerService.modifyOwner();
                break;
            }
            case "9": {
                ownerService.removeOwner();
                break;
            }
            case "10": {
                petService.addPet();
                break;
            }
            case "11": {
                petService.deletePet();
                break;
            }
            case "12": {
                petService.displayPets();
                break;
            }
            case "13": {
                vetService.scheduleConsult();
                break;
            }
            default: {
                inputOutputService.displayErrorMessage();
            }
        }

    }
}
