package com.petclinic.service;

import com.petclinic.model.Owner;
import com.petclinic.model.Vet;
import com.petclinic.repository.OwnerRepository;

import java.util.List;
import java.util.Optional;

public class OwnerService {
    private InputOutputService inputOutputService;
    private OwnerRepository ownerRepository;
    public OwnerService(InputOutputService inputOutputService) {
        this.inputOutputService = inputOutputService;
        this.ownerRepository = new OwnerRepository();
    }

    public void addOwner() {
        inputOutputService.displayGenericInputMessage("owner's first name");
        String ownerFirstName = inputOutputService.getUserInput();
        inputOutputService.displayGenericInputMessage("owner's last name");
        String ownerLastName = inputOutputService.getUserInput();
        inputOutputService.displayGenericInputMessage("owner's phone number");
        String ownerPhoneNumber = inputOutputService.getUserInput();

        Optional<Owner> optionalOwner = ownerRepository.findOwnerByPhoneNo(ownerPhoneNumber);
        if (optionalOwner.isPresent()){
            inputOutputService.phoneNoNotUniqueErrorMessage();
            return;
        }

        Owner owner = Owner.builder()
                .firstName(ownerFirstName)
                .lastName(ownerLastName)
                .phoneNo(ownerPhoneNumber)
                .build();

        ownerRepository.save(owner);
        inputOutputService.displaySuccessMessage();
    }


    public void displayOwners() {
        List<Owner> owners = ownerRepository.getAllOwners();
        inputOutputService.displayOwners(owners);
    }

    public void modifyOwner() {
        String phoneNo = getPhoneNoFromUser();
        Optional<Owner> optionalOwner = ownerRepository.findOwnerByPhoneNo(phoneNo);

        if(optionalOwner.isEmpty()){
            inputOutputService.displayErrorMessage();
            return;
        }

        Owner owner = optionalOwner.get();
        updateOwnerWithNewValues(owner);
        ownerRepository.update(owner);
    }

    private void updateOwnerWithNewValues(Owner owner) {
        inputOutputService.editMessage("first name", owner.getFirstName());
        String newFirstName = inputOutputService.getUserInput();
        inputOutputService.editMessage("last name", owner.getLastName());
        String newLastName = inputOutputService.getUserInput();
        inputOutputService.editMessage("phone number", owner.getPhoneNo());
        String newPhoneNo = inputOutputService.getUserInput();
        owner.setFirstName(newFirstName);
        owner.setLastName(newLastName);
        owner.setPhoneNo(newPhoneNo);
    }

    private String getPhoneNoFromUser() {
        inputOutputService.displayGenericInputMessage("owner's phone number");
        String phoneNo = inputOutputService.getUserInput();
        return phoneNo;
    }


    public void removeOwner() {
        String phoneNo = getPhoneNoFromUser();
        Optional<Owner> optionalOwner = ownerRepository.findOwnerByPhoneNo(phoneNo);

        if(optionalOwner.isEmpty()){
            inputOutputService.displayErrorMessage();
            return;
        }

        Owner owner = optionalOwner.get();
        ownerRepository.delete(owner);
    }
}
