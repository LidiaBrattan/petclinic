package com.petclinic.service;

import com.petclinic.enums.Type;
import com.petclinic.model.Appointment;
import com.petclinic.model.Holiday;
import com.petclinic.model.Pet;
import com.petclinic.model.Vet;
import com.petclinic.repository.AppointmentRepository;
import com.petclinic.repository.HolidayRepository;
import com.petclinic.repository.PetRepository;
import com.petclinic.repository.VetRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

public class VetService {
    private InputOutputService inputOutputService;
    private VetRepository vetRepository;
    private HolidayRepository holidayRepository;
    private PetRepository petRepository;
    private AppointmentRepository appointmentRepository;

    public VetService(InputOutputService inputOutputService) {
        this.inputOutputService = inputOutputService;
        this.vetRepository = new VetRepository();
        this.holidayRepository = new HolidayRepository();
        this.petRepository = new PetRepository();
        this.appointmentRepository = new AppointmentRepository();
    }

    public void addVet() {
        inputOutputService.displayGenericInputMessage("vet's first name");
        String vetFirstName = inputOutputService.getUserInput();
        inputOutputService.displayGenericInputMessage("vet's last name");
        String vetLastName = inputOutputService.getUserInput();
        inputOutputService.displaySpecialitiesInputMessage();
        String vetSpecialities = inputOutputService.getUserInput();
        List<Type> specialitiesList = convertToEnum(vetSpecialities);

//        Vet vet = new Vet();
//        vet.setFirstName(vetFirstName);
//        vet.setLastName(vetLastName);

        Vet vet = Vet.builder()
                .firstName(vetFirstName)
                .lastName(vetLastName)
                .specialities(specialitiesList)
                .badgeId(new Random().nextInt(9000) + 1000)
                .build();
        vetRepository.save(vet);
        inputOutputService.displaySuccessMessage();
    }

    private List<Type> convertToEnum(String vetSpecialities) {
        List<Type> result = new ArrayList<>();
        String[] specialitiesArray = vetSpecialities.split(" ");
        for(String speciality : specialitiesArray){
            try{
                Type type = Type.valueOf(speciality);
                result.add(type);
            }catch (IllegalArgumentException exception){
                inputOutputService.exceptionMessage(speciality);
            }
        }
        return result;
    }

    public void modifyVet() {
        Integer badgeId = getBadgeIdFromUser();
        Optional<Vet> optionalVet = vetRepository.findVetByBadgeId(badgeId);

        if(optionalVet.isEmpty()){
            inputOutputService.displayErrorMessage();
            return;
        }

        Vet vet = optionalVet.get();
        updateVetWithNewValues(vet);
        vetRepository.update(vet);

    }

    private Integer getBadgeIdFromUser() {
        inputOutputService.displayGenericInputMessage("vet's badge ID");
        String vetBadgeId = inputOutputService.getUserInput();
        Integer badgeId = Integer.parseInt(vetBadgeId); //convertim String in Integer
        return badgeId;
    }

    private void updateVetWithNewValues(Vet vet) {
        inputOutputService.editMessage("first name", vet.getFirstName());
        String newFirstName = inputOutputService.getUserInput();
        inputOutputService.editMessage("last name", vet.getLastName());
        String newLastName = inputOutputService.getUserInput();
        inputOutputService.editMessage("badge id", vet.getBadgeId().toString());//transformi ca String pt afisare
        String newBadgeId = inputOutputService.getUserInput();
        vet.setFirstName(newFirstName);
        vet.setLastName(newLastName);
        vet.setBadgeId(Integer.parseInt(newBadgeId)); //ca sa il recunoasca ca integer
    }

    public void removeVet() {
        Integer vetBadgeId = getBadgeIdFromUser();
        Optional<Vet> optionalVet = vetRepository.findVetByBadgeId(vetBadgeId);

        if(optionalVet.isEmpty()){
            inputOutputService.displayErrorMessage();
            return;
        }

        Vet vet = optionalVet.get();
        vetRepository.delete(vet);
    }

    public void displayVets() {
        List<Vet> vets = vetRepository.getAllVets();
        inputOutputService.displayVets(vets);
    }

    public void addHoliday() {
        Integer badgeIdFromUser = getBadgeIdFromUser();
        inputOutputService.displayGenericInputMessage("vet's holiday date in yyyy-mm-dd format: ");
        String holidayDateAsString = inputOutputService.getUserInput();


        Optional<Vet> optionalVet = vetRepository.findVetByBadgeId(badgeIdFromUser);//iti da un vet
        if (optionalVet.isEmpty()){
            inputOutputService.displayErrorMessage();
            return;
        }
        Holiday holiday = getHolidayObject(holidayDateAsString);
        Vet vet = optionalVet.get();
        linkHolidayAndVet(holiday, vet);
        vetRepository.update(vet);
    }

    private void linkHolidayAndVet(Holiday holiday, Vet vet) {
        vet.add(holiday);
        holiday.add(vet);
    }

    private Holiday getHolidayObject(String holidayDateAsString) {
        LocalDate holidayDate = LocalDate.parse(holidayDateAsString);//transforma stringul in date
        Optional<Holiday> optionalHoliday = holidayRepository.findByDate(holidayDate);
        Holiday holiday;
        if(optionalHoliday.isEmpty()){
            holiday = Holiday.builder()
                    .date(holidayDate)
                    .build();
        } else {
            holiday = optionalHoliday.get();
        }
        return holiday;
    }

    public void scheduleConsult() {
        inputOutputService.displayGenericInputMessage("the collar ID of your pet");
        String collarId = inputOutputService.getUserInput();
        Optional<Pet> optionalPet = petRepository.findPetByCollarId(collarId);

        if (optionalPet.isEmpty()){
            inputOutputService.displayErrorMessage();
            return;
        }

        Pet pet = optionalPet.get();
        List<Vet> vets = vetRepository.findVetsByPetType(pet.getType());
        inputOutputService.displayAvailableVets(vets);
        String badgeId = inputOutputService.getUserInput();

        Optional<Vet> optionalVet = getVetByBadgeId(vets, badgeId);

        if (optionalVet.isEmpty()){
            inputOutputService.displayErrorMessage();
            return;
        }

        Vet vet = optionalVet.get();
        inputOutputService.displayGenericInputMessage("the date you want for consult in yyyy-mm-dd format");
        String dateForConsultString = inputOutputService.getUserInput();
        LocalDate dateForConsult = LocalDate.parse(dateForConsultString);
        Boolean isDateUnavailable = isDateUnavailable(dateForConsult, vet.getHolidays());

        if (isDateUnavailable){
            inputOutputService.dateNotAvailableMessage();
            return;
        }

        inputOutputService.displayGenericInputMessage("the hour for the consult");
        String hourAsSTring = inputOutputService.getUserInput();
        LocalTime hour = LocalTime.parse(hourAsSTring);
        LocalDateTime dateTime = dateForConsult.atTime(hour);
        Boolean isDateAndTimeUnavailable = isDateAndTimeUnavailable(dateTime, vet.getAppointments());
        if (isDateAndTimeUnavailable){
            inputOutputService.unavailableTimeSlotMessage();
            return;
        }
        Appointment appointment = Appointment.builder()
                .vet(vet)
                .pet(pet)
                .dateTime(dateTime)
                .build();
        appointmentRepository.save(appointment);
     }

    private Boolean isDateAndTimeUnavailable(LocalDateTime dateTime, List<Appointment> appointments) {
        return appointments.stream()
                .anyMatch(appointment -> appointment.getDateTime().equals(dateTime));
    }

    private Boolean isDateUnavailable(LocalDate dateForConsult, List<Holiday> holidays) {
        return holidays.stream()
                .anyMatch(holiday -> holiday.getDate().equals(dateForConsult));
    }

    private Optional<Vet> getVetByBadgeId(List<Vet> vets, String badgeId) {
        try {
            Integer badgeIdInteger = Integer.valueOf(badgeId);
            return vets.stream()
                    .filter(vet -> vet.getBadgeId().equals(badgeIdInteger))
                    .findFirst();

        } catch (IllegalArgumentException e){
            return Optional.empty();
        }


    }
}
