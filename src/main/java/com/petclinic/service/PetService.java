package com.petclinic.service;

import com.petclinic.enums.Type;
import com.petclinic.model.Owner;
import com.petclinic.model.Pet;
import com.petclinic.model.Vet;
import com.petclinic.repository.OwnerRepository;
import com.petclinic.repository.PetRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Random;

public class PetService {
    private InputOutputService inputOutputService;
    private OwnerRepository ownerRepository;
    private PetRepository petRepository;

    public PetService(InputOutputService inputOutputService) {
        this.inputOutputService = inputOutputService;
        this.ownerRepository = new OwnerRepository();
        this.petRepository = new PetRepository();
    }

    public void addPet() {
        inputOutputService.displayGenericInputMessage("pet's name");
        String name = inputOutputService.getUserInput();
        inputOutputService.displayGenericInputMessage("pet's type");
        String petType = inputOutputService.getUserInput();
        Optional<Type> optionalType = convertToEnum(petType);

        if (optionalType.isEmpty()) {
            inputOutputService.displayErrorMessage();
            return;
        }

        inputOutputService.displayGenericInputMessage("pet's birth date");
        String birthDate = inputOutputService.getUserInput();
        inputOutputService.displayGenericInputMessage("if pet is vaccinated (Yes/No)");
        String isVaccinatedString = inputOutputService.getUserInput();
        Optional<Boolean> optionalBoolean = toOptionalBoolean(isVaccinatedString);

        if (optionalBoolean.isEmpty()) {
            inputOutputService.displayErrorMessage();
            return;
        }

        inputOutputService.displayGenericInputMessage("owner's phone number");
        String ownerPhoneNo = inputOutputService.getUserInput();
        Optional<Owner> optionalOwner = ownerRepository.findOwnerByPhoneNo(ownerPhoneNo);

        if (optionalOwner.isEmpty()) {
            inputOutputService.ownerDoesNotExistMessage();
            return;
        }

        Pet pet = Pet.builder()
                .name(name)
                .type(optionalType.get())
                .birthDate(LocalDate.parse(birthDate))
                .isVaccinated(optionalBoolean.get())
                .owner(optionalOwner.get())
                .collarId(generateCollarId())
                .build();

        petRepository.save(pet);
        inputOutputService.displaySuccessMessage();
    }

    private String generateCollarId() {
        Random random = new Random();
        String result = " ";

        for (int i = 1; i <= 4; i++) {
            int randomInt = random.nextInt(26) + 65;
            result = result + Character.toString(randomInt);
        }
        return result;
    }

    public Optional<Boolean> toOptionalBoolean(String yesNoString) {

        if (yesNoString.equalsIgnoreCase("yes")) {
            return Optional.of(true);
        } else if (yesNoString.equalsIgnoreCase("no")) {
            return Optional.of(false);
        }
        return Optional.empty();
    }

    private Optional<Type> convertToEnum(String petType) {
        try {
            Type type = Type.valueOf(petType);
            return Optional.of(type);
        } catch (IllegalArgumentException exception) {
            return Optional.empty();
        }
    }

    public void deletePet() {
        inputOutputService.displayGenericInputMessage("pet's collar Id");
        String collarId = inputOutputService.getUserInput();
        Optional<Pet> optionalPet = petRepository.findPetByCollarId(collarId);
        if (optionalPet.isEmpty()) {
            inputOutputService.petDoesNotExistessage();
            return;
        }
        petRepository.delete(optionalPet.get());
        inputOutputService.displaySuccessMessage();
    }

    public void displayPets() {
        List<Pet> petList = petRepository.getAllPets();
        inputOutputService.displayPets(petList);
    }


}
