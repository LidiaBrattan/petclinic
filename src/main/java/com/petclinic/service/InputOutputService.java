package com.petclinic.service;

import com.petclinic.enums.Type;
import com.petclinic.model.Owner;
import com.petclinic.model.Pet;
import com.petclinic.model.Vet;

import java.util.List;
import java.util.Scanner;

public class InputOutputService {

    public void displayMainMenu() {
        System.out.println("\nWelcome to Pet Clinic!");
        System.out.println("Please choose one of the following options:");
        System.out.println("1 - add a new vet");
        System.out.println("2 - display all vets");
        System.out.println("3 - modify vet details");
        System.out.println("4 - remove an existing vet");
        System.out.println("5 - add vet holiday");
        System.out.println("----------------------------");
        System.out.println("6 - add new owner");
        System.out.println("7 - display all owners");
        System.out.println("8 - modify owner details");
        System.out.println("9 - remove an existing owner");
        System.out.println("----------------------------");
        System.out.println("10 - add a new pet");
        System.out.println("11 - delete pet");
        System.out.println("12 - display all pets");
        System.out.println("----------------------------");
        System.out.println("13 - schendule a consult");
    }

    public String getUserInput() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Your input: ");
        String userInput = scanner.nextLine();
        return userInput;
    }

    public void displayErrorMessage() {
        System.out.println("Sorry, the input is not valid. Please try again.");
    }

    public void displayGenericInputMessage(String message) {
        System.out.println("Please insert " + message);
    }

    public void displaySuccessMessage() {
        System.out.println("Operation successfully completed!");
    }

    public void displaySpecialitiesInputMessage() {
        System.out.print("Please insert vet's specialities separated by a single space from the following: ");
        for (Type type : Type.values()) {
            System.out.print(type.name() + " ");
        }
    }

    public void exceptionMessage(String speciality) {
        System.out.println("Invalid type: " + speciality);
    }


    public void displayVets(List<Vet> vets) {
        System.out.println("The Pet Clinic has the following vets: ");
        for (Vet vet : vets) {
            System.out.println(vet.getFirstName() + " " + vet.getLastName() + " " + vet.getBadgeId());
        }
    }

    public void editMessage(String fieldName, String fieldValue) {
        System.out.println("Current " + fieldName + " is " + fieldValue + ". " + "What is the new value?");
    }

    public void phoneNoNotUniqueErrorMessage() {
        System.out.println("Sorry, the phone number is already registered to another owner!");
    }

    public void displayOwners(List<Owner> owners) {
        System.out.println("The Pet Clinic has the following owners registered: ");
        for (Owner owner : owners) {
            System.out.println(owner.getFirstName() + " " + owner.getLastName() + " " + owner.getPhoneNo());
        }
    }

    public void ownerDoesNotExistMessage() {
        System.out.println("There is no owner with this phone number!");
    }

    public void petDoesNotExistessage() {
        System.out.println("There is no pet with this collar Id!");
    }

    public void displayPets(List<Pet> petList) {
        for (Pet pet : petList){
            System.out.println(pet.getName() + ", "
                    + pet.getCollarId() + " - "
                    + pet.getOwner().getFirstName() + " "
                    + pet.getOwner().getLastName());
        }
    }

    public void displayAvailableVets(List<Vet> vets) {
        System.out.println("Please insert the badge ID from the vet you desire: ");
        for (Vet vet : vets){
            System.out.println(vet.getBadgeId() + " - " + vet.getFirstName() + " " + vet.getLastName());
        }
    }

    public void dateNotAvailableMessage() {
        System.out.println("Sorry, this date is not available, the vet is in holiday");
    }

    public void unavailableTimeSlotMessage() {
        System.out.println("There is already a consult schenduled at that time");
    }
}
