package com.petclinic;

import com.petclinic.util.HibernateUtil;

public class Main {

    public static void main(String[] args) {
        PetClinic petClinic = new PetClinic();
        petClinic.start();
    }
}
