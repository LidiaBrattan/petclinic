package com.petclinic.model;

import com.petclinic.enums.Type;
import lombok.*;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Vet {
    @Id
    @GeneratedValue
    private Integer vetId;

    private String firstName;
    private String lastName;

    @Column(unique = true)
    private Integer badgeId;

    //asa se procedeaza in cazul unei relatii de many to many cu un ENUM
    @ElementCollection(targetClass = Type.class)
    @Enumerated(value = EnumType.STRING)//salveaza enumurile ca stringuri in baza de date
    @CollectionTable(name = "vet_type",
            joinColumns = @JoinColumn(name = "vetId"))
    @OnDelete(action = OnDeleteAction.CASCADE)
    @Cascade(value = {org.hibernate.annotations.CascadeType.ALL})
    @JoinColumn
    private List<Type> specialities;

    @OneToMany (mappedBy = "vet")
    private List<Appointment> appointments;

    @ManyToMany(mappedBy = "vets", cascade = CascadeType.ALL)
    private List<Holiday> holidays;

    public void add(Holiday holiday) {
        if (holidays == null){
            holidays = new ArrayList<>();
        }
        holidays.add(holiday);
    }
}
