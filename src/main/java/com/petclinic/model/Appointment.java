package com.petclinic.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
@Entity
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Appointment {
    @Id
    @GeneratedValue
    private Integer appointmentId;

    private LocalDateTime dateTime;
    private Boolean isCancelled;

    @ManyToOne
    @JoinColumn
    private Pet pet;

    @ManyToOne
    @JoinColumn
    private Vet vet;
}
