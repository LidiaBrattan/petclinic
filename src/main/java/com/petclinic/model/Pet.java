package com.petclinic.model;

import com.petclinic.enums.Type;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Pet {
    @Id
    @GeneratedValue
    private Integer petId;

    private String name;
    private Type type;
    private LocalDate birthDate;
    private Boolean isVaccinated;

    @Column(unique = true)
    private String collarId;

    @ManyToOne
    @JoinColumn
    private Owner owner;

    @OneToMany (mappedBy = "pet")
    private List<Appointment> appointments;
}
