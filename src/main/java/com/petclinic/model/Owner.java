package com.petclinic.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Owner {
    @Id
    @GeneratedValue
    private Integer ownerId;

    private String firstName;
    private String lastName;

    @Column(unique = true)
    private String phoneNo;

    @OneToMany (mappedBy = "owner")
    private List<Pet> pets;
}
