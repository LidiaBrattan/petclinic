package com.petclinic.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Holiday {

    @Id
    @GeneratedValue
    private Integer holidayId;
    private LocalDate date;

    @ManyToMany
    @JoinTable(name = "holiday_vet",
            joinColumns = @JoinColumn(name = "holidayId"),
            inverseJoinColumns = @JoinColumn(name = "vetId")
    )
    private List<Vet> vets;

    public void add(Vet vet) {
        if (vets == null){
            vets = new ArrayList<>();
        }
        vets.add(vet);
    }
}
