package com.petclinic.util;
import com.petclinic.model.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import java.util.Properties;

public class HibernateUtil {

    private static SessionFactory instance;

    public static SessionFactory getInstance() {
        if( instance == null){
            instantiateSessionFactory();
        }
        return instance;
    }

    private static void instantiateSessionFactory() {
        /*START BOILER PLATE CODE */
        Configuration configuration = new Configuration();
        Properties settings = new Properties();
        settings.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");

        settings.put(Environment.URL, "jdbc:mysql://localhost:3306/petclinic?serverTimezone=UTC&useSSL=false&allowPublicKeyRetrieval=true");
        settings.put(Environment.USER, "root");
        settings.put(Environment.PASS, "root");
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL55Dialect");

        settings.put(Environment.SHOW_SQL, "true");
        settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
        settings.put(Environment.HBM2DDL_AUTO, "update");


        configuration.addAnnotatedClass(Vet.class);
        configuration.addAnnotatedClass(Pet.class);
        configuration.addAnnotatedClass(Owner.class);
        configuration.addAnnotatedClass(Consult.class);
        configuration.addAnnotatedClass(Appointment.class);
        configuration.addAnnotatedClass(Holiday.class);


        configuration.setProperties(settings);

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties()).build();
        instance = configuration.buildSessionFactory(serviceRegistry);
        /*END BOILER PLATE CODE*/
    }
}
