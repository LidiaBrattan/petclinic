package com.petclinic.enums;

public enum Type {
    DOG, CAT, PARROT, FISH, GENERIC
}
