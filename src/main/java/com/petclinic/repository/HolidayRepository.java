package com.petclinic.repository;

import com.petclinic.model.Holiday;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.time.LocalDate;
import java.util.Optional;

public class HolidayRepository extends GenericRepository<Holiday> {

    public Optional<Holiday> findByDate(LocalDate holidayDate) {
        Session session = sessionFactory.openSession();
        Query<Holiday> query = session.createQuery("select h from Holiday h where h.date = :holiday");
        query.setParameter("holiday", holidayDate);
        return query.uniqueResultOptional();
    }
}
