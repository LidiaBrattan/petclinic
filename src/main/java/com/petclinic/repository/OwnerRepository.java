package com.petclinic.repository;

import com.petclinic.model.Owner;
import com.petclinic.model.Vet;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;
import java.util.Optional;

public class OwnerRepository extends GenericRepository<Owner> {

    public Optional<Owner> findOwnerByPhoneNo(String phoneNo) {
        Session session = sessionFactory.openSession();
        Query<Owner> query = session.createQuery("select o from Owner o where o.phoneNo = :phoneNo");
        query.setParameter("phoneNo", phoneNo);
        return query.uniqueResultOptional();
    }

    public List<Owner> getAllOwners() {
        Session session = sessionFactory.openSession();
        Query<Owner> query = session.createQuery("select o from Owner o");
        return query.list();
    }
}
