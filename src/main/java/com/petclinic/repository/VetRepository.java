package com.petclinic.repository;

import com.petclinic.enums.Type;
import com.petclinic.model.Vet;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;
import java.util.Optional;

public class VetRepository extends GenericRepository<Vet>{

    public List<Vet> getAllVets() {
        Session session = sessionFactory.openSession();
        Query<Vet> query = session.createQuery("select v from Vet v");
        return query.list();
    }

    public Optional<Vet> findVetByBadgeId(Integer vetBadgeId) {
        Session session = sessionFactory.openSession();
        Query<Vet> query = session.createQuery("select v from Vet v where v.badgeId = :badgeId");
        query.setParameter("badgeId", vetBadgeId);
        return query.uniqueResultOptional();
    }


    public List<Vet> findVetsByPetType(Type type) {
        Session session = sessionFactory.openSession();
        Query<Vet> query = session.createQuery("select v from Vet v");
//        query.setParameter("type", type.name());
        return query.list();
    }
}
