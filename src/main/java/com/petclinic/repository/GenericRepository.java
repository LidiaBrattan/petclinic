package com.petclinic.repository;

import com.petclinic.util.HibernateUtil;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class GenericRepository<X> {
    protected SessionFactory sessionFactory = HibernateUtil.getInstance();


    public void save(X object) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(object);
        transaction.commit();
        session.close();
    }

    //nu merge cu update si delete pt ca sesiunea este DETACHED -->look it up
    public void update(X object) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.merge(object);
        transaction.commit();
        session.close();
    }

    public void delete(X object) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Object merged = session.merge(object);
        session.delete(merged);
        transaction.commit();
        session.close();
    }
}
