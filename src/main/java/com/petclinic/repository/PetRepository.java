package com.petclinic.repository;

import com.petclinic.model.Pet;
import com.petclinic.model.Vet;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;
import java.util.Optional;

public class PetRepository extends GenericRepository<Pet> {

    public Optional<Pet> findPetByCollarId(String collarId) {
        Session session = sessionFactory.openSession();
        Query<Pet> query = session.createQuery("select p from Pet p where p.collarId = :collarId");
        query.setParameter("collarId", collarId);
        return query.uniqueResultOptional();
    }

    public List<Pet> getAllPets() {
        Session session = sessionFactory.openSession();
        Query<Pet> query = session.createQuery("select p from Pet p");
        return query.list();
    }
}
